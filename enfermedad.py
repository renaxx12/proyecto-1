class Enfermedad():
    def __init__(self, infeccion_probable, promedio_pasos):

        self.__prob_inf = infeccion_probable
        self.__prom_pasos = promedio_pasos
        self.__enfermo = False

    @property
    def prob_inf(self):
        return self.__prob_inf

    @prob_inf.setter
    def prob_inf(self, infeccion_probable):
        if isinstance(infeccion_probable, float):
            self.__prob_inf = infeccion_probable
        else:
            print("El tipo de dato no corresponde")

    @property
    def prom_pasos(self):
        return self.__prom_pasos

    @prom_pasos.setter
    def prom_pasos(self, promedio_pasos):
        if isinstance(promedio_pasos, int):
            self.__comunidad = promedio_pasos
        else:
            print("El tipo de dato no corresponde")

    @property
    def enfermedad(self):
        return self.__enfermo
