from enfermedad import Enfermedad

class Persona():
    def __init__(self, run, comunidad):
        self.__comunidad = comunidad
        self.__run = run
        self.__familia = []
        self.__enfermedad = None
        self.__estado = True #True es sano o muerto, False contagiado
        self.__inmunidad = False
        self.__dias_enfermo = None

    @property
    def comunidad(self):
        return self.__comunidad

    @comunidad.setter
    def comunidad(self, comunidad):
        if isinstance(comunidad, str):
            self.__comunidad = comunidad
        else:
            print("El tipo de dato no corresponde")

    @property
    def run(self):
        return self.__run

    @run.setter
    def run(self, run):
        if isinstance(run, int):
            self.__run = run
        else:
            print("El tipo de dato no corresponde")

    @property
    def familia(self):
        return self.__familia

    @familia.setter
    def familia(self, familiar):
        if isinstance(familiar, Persona):
            self.__familia.append(familiar)
        else:
            print("El tipo de dato no corresponde")

    @property
    def enfermedad(self):
        return self.__enfermedad

    @enfermedad.setter
    def enfermedad(self, virus):
        if isinstance(virus, Enfermedad):
            self.__enfermedad = virus
        else:
            print("El tipo de dato no corresponde")

    @property
    def estado(self):
        return self.__estado

    @estado.setter
    def estado(self, valor):
        if isinstance(valor, bool):
            self.__estado = valor

    @property
    def inmunidad(self):
        return self.__inmunidad

    @property
    def dias_enfermo(self):
        return self.__dias_enfermo

    #se suman los dias de enfermedad, se cambian los atributos segun corresponda
    def dia_siguiente(self):
        if self.dias_enfermo < self.__comunidad.enfermedad.prom_pasos:
            self.dias_enfermo += 1
        else:
            self.__inmunidad = True
            self.__estado = True

    @dias_enfermo.setter
    def dias_enfermo(self, valor):
        if isinstance(valor, int):
            self.__dias_enfermo = valor

