from persona import Persona
from comunidad import Comunidad
import random as r

class Simulacion():
    def __init__(self, comunidad):

        self.__comunidad = comunidad
        self.lista_recuperados = []

    def crear_comunidad(self):
        #se crean los objetos tipo persona
        for x in range(self.__comunidad.num_ciu):
            persona = Persona(x, self.__comunidad)
            self.__comunidad.list_cui = persona

    #se revuelve la lista y se divide en grupos para asignarle 
    #familias a las personas.
    #en el siguiente proyecto se implementará el porcentaje de contagio
    #según la pertenencia a un grupo familiar
    def crear_grupos(self):
        r.shuffle(self.__comunidad.list_cui)

        lista = []
        for x in self.__comunidad.list_cui:
            lista.append(x)
            if len(lista) == 5:
                for x in self.__comunidad.list_cui:
                    if x in lista:
                        for h in lista:
                            x.familia = h
                lista = []
    
    #se revuelve de nuevo la lista y se contagia a esa porcion de la poblacion
    #y se crea una lista para contener a esa gente
    def crear_enfermos_iniciales(self):
        r.shuffle(self.__comunidad.list_cui)
        self.lista_enfermos = []
        
        for x in self.__comunidad.list_cui:
            if len(self.lista_enfermos) < self.__comunidad.num_infe:
                self.lista_enfermos.append(x)
                x.estado = False
                x.dias_enfermo = 0
                x.enfermedad = self.__comunidad.enfermedad

    #con la lista nuevamente revuelta se toma otra porcion de gente, asegurandose
    #de que esté sana
    def seleccionar_gente_sana(self):
        r.shuffle(self.__comunidad.list_cui)
        self.lista_susceptible = []
        cantidad_gente = r.randint(10,15)
        for x in self.__comunidad.list_cui:
            if len(self.lista_susceptible) < cantidad_gente:
                if x.estado:
                    self.lista_susceptible.append(x)

    #se hace que cada persona enferma tenga la posibilidad de interactuar con la gente contagiada
    #si se reunen, se ve la probabilidad de que esta se contagie, comprobando su inmunidad
    def juntar_gente(self):
        recorrido_inicial = len(self.lista_enfermos)
        for x in range(recorrido_inicial):
            for i in self.lista_susceptible:
                if i.estado:
                    probabilidad_contacto = r.random()
                    if probabilidad_contacto <= self.__comunidad.prob_com_fis:
                        probabilidad_contagio = r.random()
                        if probabilidad_contagio <= self.__comunidad.enfermedad.prob_inf:
                            if not i.inmunidad:
                                i.estado = False
                                self.lista_enfermos.append(i)
                                i.dias_enfermo = 0
                                i.enfermedad = self.__comunidad.enfermedad

    #lleva la cuenta de los dias que una persona puede estar enferma
    #luego al llegar al ultimo dia se remueve de la lista de enfermos para
    #formar parte de la de recuperados
    def monitorear_enfermos(self):
        for x in self.lista_enfermos:
            x.dia_siguiente()
            if x.estado:
                self.lista_enfermos.remove(x)
                self.lista_recuperados.append(x)

    #se saca la cuenta del total de la jornada, usando las listas para saber
    #la cantidad de gente
    def total_dia(self):
        total_enfermos = len(self.lista_enfermos)
        total_recuperados = len(self.lista_recuperados)
        total_sanos = 0
        for i in self.__comunidad.list_cui:
            if i.estado and not i.inmunidad:
                total_sanos += 1
        print("-"*20)
        print("el total de enfermos es: ",total_enfermos)
        print("el total de recuperados es: ",total_recuperados)
        print("el total de sanos es: ", total_sanos)
        print("-"*20)
                    

    