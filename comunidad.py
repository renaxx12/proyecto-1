from persona import Persona

class Comunidad():
    def __init__(self, num_ciudadanos, promdeio_conexion_fisica,
                 enfermedad, num_infectados, probabilidad_conexion_fisica):
        self.__num_ciu = num_ciudadanos
        self.__prom_con_fis = promdeio_conexion_fisica
        self.__enfermedad = enfermedad
        self.__num_infe = num_infectados
        self.__prob_com_fis = probabilidad_conexion_fisica
        self.__list_cui = []


    @property
    def num_ciu(self):
        return self.__num_ciu

    @num_ciu.setter
    def num_ciu(self, num_ciudadanos):
        if isinstance(num_ciudadanos, int):
            self.__num_ciu = num_ciudadanos
        else:
            print("El tipo de dato no corresponde")
            
    @property
    def prom_con_fis(self):
        return self.__prom_con_fis

    @prom_con_fis.setter
    def prom_con_fis(self, promdeio_conexion_fisica):
        if isinstance(promdeio_conexion_fisica, int):
            self.__prom_con_fis = promdeio_conexion_fisica
        else:
            print("El tipo de dato no corresponde")

    @property
    def enfermedad(self):
        return self.__enfermedad

    @enfermedad.setter
    def enfermedad(self, enfermedad):
        if isinstance(enfermedad, str):
            self.__enfermedad = enfermedad
        else:
            print("El tipo de dato no corresponde")

    @property
    def num_infe(self):
        return self.__num_infe

    @num_infe.setter
    def num_infe(self, num_infectados):
        if isinstance(num_infectados, int):
            self.__num_infe = num_infectados
        else:
            print("El tipo de dato no corresponde")

    @property
    def prob_com_fis(self):
        return self.__prob_com_fis

    @prob_com_fis.setter
    def prob_com_fis(self, probabilidad_conexion_fisica):
        if isinstance(probabilidad_conexion_fisica, float):
            self.__prob_com_fis = probabilidad_conexion_fisica
        else:
            print("El tipo de dato no corresponde")

    @property
    def list_cui(self):
        return self.__list_cui

    @list_cui.setter
    def list_cui(self, persona):
        if isinstance(persona, Persona):
            self.__list_cui.append(persona)
        else:
            print("el tipo de dato no corresponde")
