from enfermedad import Enfermedad
from comunidad import Comunidad
from simulacion import Simulacion

def main():

    covid = Enfermedad(infeccion_probable=0.3,
                       promedio_pasos=18)

    talca = Comunidad(num_ciudadanos=200,
                      promdeio_conexion_fisica=8,
                      enfermedad=covid,
                      num_infectados=15,
                      probabilidad_conexion_fisica=0.8)

    sim = Simulacion(talca)
    sim.crear_comunidad()
    sim.crear_grupos()
    sim.crear_enfermos_iniciales()
    #valores arbitrarios de un ciclo
    for x in range(35):
        print("DIA ", x+1)
        sim.seleccionar_gente_sana()
        sim.juntar_gente()
        sim.monitorear_enfermos()
        sim.total_dia()


if __name__ == '__main__':
    main()
